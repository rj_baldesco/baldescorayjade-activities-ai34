<?php

namespace App\Http\Controllers;

use App\Models\Patron;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Requests\PatronRequest;

class PatronController extends Controller
{
    //

    
    public function index()
    {
        return response()->json(Patron::all());
    }

    public function store(PatronRequest $request)

    {
        return response()->json(Patron::create($request->all()));
    }

    public function show($id)
    {
        try{
        return response()->json(Patron::findOrFail($id));
        }catch(ModelNotFoundException $exception){
            return response() ->json(['message' => 'Patron not Found']);
        }
    }

    public function update(PatronRequest $request, $id)
    {
        $patron = Patron::where('id', $id)->firstOrFail();
        $patron->update($request->all());

        return response()->json(['message' => 'Patron updated','patron'=>$patron]);
    }

    public function destroy($id)
    {
        try{
        $patron = Patron::where('id', $id)->firstOrFail();
        $patron->delete();

        return response()->json(['message' => 'Patron deleted successfully!']);
        }catch(ModelNotFoundException $exception){
            return response() ->json(['message' => 'Patron not Found']);
        }
    }

    
}
