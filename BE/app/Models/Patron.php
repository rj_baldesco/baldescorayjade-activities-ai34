<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Patron extends Model
{
    use HasFactory;

    protected $fillable = ['last_name', 'first_name', 'middle_name', 'email'];

    public function borrowedBook()
    {
        return $this->hasMany(BorrowedBook::class, 'patron_id', 'id');
    }

    public function returnedBook()
    {
        return $this->hasMany(ReturnedBook::class, 'patron_id', 'id');
    }

}
